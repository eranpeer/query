package org.query;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class SelectStatementBase {

	protected static <T> T makeEntity(final DbRowImpl dbRow, DbEntityMetadata dbEntityMetadata,
			PropertyMetadata[] properties) throws InstantiationException, IllegalAccessException, Exception {
		T e = (T) dbEntityMetadata.getEntityType().newInstance();
		QueryUtils.populate(e, dbRow, properties);
		return e;
	}

	protected static <T> List<T> collectAll(ResultSet rs, DbEntityMetadata dbEntityMetadata, PropertyMetadata[] properties) {
		List<T> result = new ArrayList<T>();
		try {
			final DbRowImpl dbRow = new DbRowImpl(rs);
			while (rs.next()) {
				T e = makeEntity(dbRow, dbEntityMetadata, properties);
				result.add(e);
			}
			return result;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected static <T> T collectOne(ResultSet rs, DbEntityMetadata t1Metadata, PropertyMetadata[] selectedProperties) {
		try {
			if (!rs.next()) {
				return null;
			}
			
			final DbRowImpl dbRow = new DbRowImpl(rs);
			
			T e = makeEntity(dbRow, t1Metadata, selectedProperties);
			
			if (rs.next())
				throw new RuntimeException("expeced one but got many");
			
			return e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}