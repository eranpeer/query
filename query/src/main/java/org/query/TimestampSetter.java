package org.query;

import java.sql.Timestamp;

public interface TimestampSetter {
	public void set(Object thiz, Timestamp val);
}