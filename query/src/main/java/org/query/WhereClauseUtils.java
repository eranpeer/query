package org.query;

import java.util.function.Consumer;

public class WhereClauseUtils<T1> {

	public static <T> SqlElement recordWhereClause(WhereClauseRecorder<T> whereClauseRecorder,
			DbEntityMetadata dbEntityMetadata) {
		WhereClauseBuilder whereClauseBuilder = new WhereClauseBuilder();
		T recordingEntity = makeRecordingEntity(dbEntityMetadata, whereClauseBuilder);
		WhereClauseBuilder.builder.set(whereClauseBuilder);
		try {
			return whereClauseRecorder.record(recordingEntity);
		} finally {
			WhereClauseBuilder.builder.set(null);
		}
	}

	public static <T1, T2> SqlElement recordWhereClause(WhereClauseRecorder2<T1, T2> whereClauseRecorder,
			DbEntityMetadata dbEntityMetadata1,DbEntityMetadata dbEntityMetadata2) {
		WhereClauseBuilder whereClauseBuilder = new WhereClauseBuilder();
		T1 recordingEntity1 = makeRecordingEntity(dbEntityMetadata1, whereClauseBuilder);
		T2 recordingEntity2 = makeRecordingEntity(dbEntityMetadata2, whereClauseBuilder);
		WhereClauseBuilder.builder.set(whereClauseBuilder);
		try {
			return whereClauseRecorder.record(recordingEntity1, recordingEntity2);
		} finally {
			WhereClauseBuilder.builder.set(null);
		}
	}

	public static <T1, T2, T3> SqlElement recordWhereClause(WhereClauseRecorder3<T1, T2, T3> whereClauseRecorder,
			DbEntityMetadata dbEntityMetadata1,DbEntityMetadata dbEntityMetadata2,DbEntityMetadata dbEntityMetadata3) {
		WhereClauseBuilder whereClauseBuilder = new WhereClauseBuilder();
		T1 recordingEntity1 = makeRecordingEntity(dbEntityMetadata1, whereClauseBuilder);
		T2 recordingEntity2 = makeRecordingEntity(dbEntityMetadata2, whereClauseBuilder);
		T3 recordingEntity3 = makeRecordingEntity(dbEntityMetadata3, whereClauseBuilder);
		WhereClauseBuilder.builder.set(whereClauseBuilder);
		try {
			return whereClauseRecorder.record(recordingEntity1, recordingEntity2, recordingEntity3);
		} finally {
			WhereClauseBuilder.builder.set(null);
		}
	}

	public static <T> SqlElement recordSetClause(SetClauseRecorder<T> setClauseRecorder,
			DbEntityMetadata dbEntityMetadata) {
		SetClauseBuilder setClauseBuilder = new SetClauseBuilder();
		T recordingEntity = makeRecordingEntity(dbEntityMetadata, setClauseBuilder);
		setClauseBuilder.builder.set(setClauseBuilder);
		try {
			setClauseRecorder.record(recordingEntity);
			return setClauseBuilder.getRecordedSetClouse();
		} finally {
			WhereClauseBuilder.builder.set(null);
		}
	}

	private static <T> T makeRecordingEntity(DbEntityMetadata dbEntityMetadata,
			Consumer<PropertyMethodInvocation> lastMethodCallConsumer) {
		return dbEntityMetadata.makeProxy(new RecordingIterceptor(lastMethodCallConsumer));
	}

}