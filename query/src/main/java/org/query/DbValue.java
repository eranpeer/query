package org.query;

import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

public interface DbValue {
	boolean asBoolean() throws Exception;

	byte asByte() throws Exception;

	byte asShort() throws Exception;

	int asInt() throws Exception;

	Integer asIntWrapper() throws Exception;

	float asFloat() throws Exception;

	long asLong() throws Exception;

	Long asLongWrapper() throws Exception;

	double asDouble() throws Exception;

	String asString() throws Exception;

	Timestamp asTimestamp() throws Exception;

	java.sql.Date asDate() throws SQLException;
	
	java.util.Date asJavaDate() throws SQLException;

	LocalDateTime asLocalDateTime() throws SQLException;

	LocalDate asLocalDate() throws SQLException;

	java.sql.Time asTime() throws SQLException;

	Blob asBlob() throws SQLException;

	byte[] asBytes() throws SQLException;

}