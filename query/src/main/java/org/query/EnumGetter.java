package org.query;

public interface EnumGetter {
	public Enum<?> get(Object thiz);
}