package org.query;

public interface BooleanSetter {
	public void set(Object thiz, boolean val);
}