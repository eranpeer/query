package org.query;

public interface LongWrapperGetter {
	public Long get(Object thiz);
}