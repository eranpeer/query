package org.query;

public interface BooleanGetter {
	public boolean get(Object thiz);
}