package org.query;

import java.util.ArrayList;

public class UpdateStatement<T1> {

	public static class ExecutableStatement<T1> {

		private final JdbcQueryEngine engine;
		private final ParsedStatement parsedStatement;

		public ExecutableStatement(JdbcQueryEngine engine, DbEntityMetadata t1Metadata,
				ParsedStatement parsedStatement) {
			this.engine = engine;
			this.parsedStatement = parsedStatement;
		}

		public int execute() {
			String statementSql = parsedStatement.getStatementSql();
			Object[] arguments = parsedStatement.getArguments();
			return engine.update(statementSql, //
					arguments, //
					null);
		}
	}

	private final JdbcQueryEngine engine;
	private WhereClauseRecorder<T1> filter;
	private DbEntityMetadata t1Metadata;
	private SetClauseRecorder<T1> changeRecorder;

	public UpdateStatement(JdbcQueryEngine engine, Class<T1> t1) {
		this.engine = engine;
		this.t1Metadata = engine.getMetadata(t1);
	}

	public int update(T1 element) {
		Object id;
		try {
			id = t1Metadata.getIdProperty().getValue(element);
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
		where(e -> new WhereClauseSqlElements.Eq(t1Metadata.getIdProperty(), id));
		set(e -> {
			PropertyMetadata[] properties = t1Metadata.getProperties();
			for (PropertyMetadata propertyMetadata : properties) {
				if (!propertyMetadata.isId()) {
					try {
						propertyMetadata.setValue(e, propertyMetadata.getValue(element));
					} catch (Exception e1) {
						throw new RuntimeException(e1);
					}
				}
			}
		});
		return makeExecutableStatement().execute();
	}

	@SuppressWarnings("unchecked")
	public ExecutableStatement<T1> makeExecutableStatement() {
		StringBuilder statementText = new StringBuilder();
		// final PropertyMetadata[] updatedProperties =
		// t1Metadata.getProperties();
		ArrayList<Object> queryParams = new ArrayList<>();
		// QueryUtils.appendUpdatedColumns(statementText, updatedProperties,
		// queryParams);

		statementText.append("update ");
		statementText.append(t1Metadata.getFormattedTableName());
		if (changeRecorder != null) {
			statementText.append("\nset ");
			SqlElement wc = WhereClauseUtils.recordSetClause(changeRecorder, t1Metadata);
			wc.apply(statementText, queryParams);
		}
		if (filter != null) {
			statementText.append("\nwhere ");
			SqlElement wc = WhereClauseUtils.recordWhereClause(filter, t1Metadata);
			wc.apply(statementText, queryParams);
		}
		ParsedStatement parsedStatement = new ParsedStatement(statementText.toString(), queryParams.toArray());
		return new ExecutableStatement<>(engine, t1Metadata, parsedStatement);
	}

	public UpdateStatement<T1> where(WhereClauseRecorder<T1> filter) {
		this.filter = filter;
		return this;
	}

	public UpdateStatement<T1> set(SetClauseRecorder<T1> changeRecorder) {
		this.changeRecorder = changeRecorder;
		return this;
	}

	public int execute() {
		return makeExecutableStatement().execute();
	}
}