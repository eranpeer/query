package org.query;

import java.sql.Time;

public interface TimeGetter {
	public Time get(Object thiz);
}