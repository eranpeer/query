package org.query;

import java.lang.reflect.Method;

import net.bytebuddy.implementation.bind.annotation.Origin;
import net.bytebuddy.implementation.bind.annotation.This;

public interface MethodInvocationInterceptor {
	Object intercept(@This Object thiz, @Origin Method method); // Getter
	void intercept(@This Object thiz, @Origin Method method, Object argument); // Setter
}