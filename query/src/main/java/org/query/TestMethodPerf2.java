package org.query;

import java.lang.invoke.CallSite;
import java.lang.invoke.LambdaMetafactory;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Field;

public class TestMethodPerf2 {
	
	private static final int ITERATIONS = 50_000_000;
	private static final int WARM_UP = 10;

	public static interface IntGetter {
		Object get();
	}

	public static interface IntSetter {
		void set(int value);
	}

	public static int f;

	public static void main(String... args) throws Throwable {
		// hold result to prevent too much optimizations
		final int[] dummy = new int[4];

		
		Field reflectedf = TestMethodPerf2.class.getDeclaredField("f");
		//reflectedf.setAccessible(true);
		
		final MethodHandles.Lookup lookup = MethodHandles.lookup();
		
		MethodHandle mhfgetter = lookup.findStaticGetter(TestMethodPerf2.class, "f", int.class);
		MethodHandle mhfsetter = lookup.findStaticSetter(TestMethodPerf2.class, "f", int.class);
		
		final CallSite metafactory = LambdaMetafactory.metafactory(
			      lookup, "get", MethodType.methodType(IntGetter.class),
			      mhfgetter.type(), mhfgetter, mhfgetter.type());
		
		IntGetter lambdaGetter = () -> {
			try {
				return (int) mhfgetter.invokeExact();
			} catch (Throwable e) {
				return 0;
			}
		};
		
		IntSetter lambdaSetter = (int val) -> {
			try {
				mhfsetter.invokeExact(val);
			} catch (Throwable e) {
			}
		};


		for (int i = 0; i < WARM_UP; i++) {
			dummy[0] += testDirect(dummy[0]);
			dummy[1] += testLambda(dummy[1], lambdaSetter, lambdaGetter);
			dummy[2] += testMH(dummy[1], mhfsetter,mhfgetter);
			dummy[3] += testReflection(dummy[2], reflectedf);
		}
		
		long t0 = System.nanoTime();
		dummy[0] += testDirect(dummy[0]);
		long t1 = System.nanoTime();
		dummy[1] += testLambda(dummy[1], lambdaSetter,lambdaGetter);
		long t2 = System.nanoTime();
		dummy[2] += testMH(dummy[1], mhfsetter,mhfgetter);
		long t3 = System.nanoTime();
		dummy[3] += testReflection(dummy[2], reflectedf);
		long t4 = System.nanoTime();
		System.out.printf("direct: %.2fs, lambda: %.2fs, mh: %.2fs, reflection: %.2fs%n", (t1 - t0) * 1e-9,
				(t2 - t1) * 1e-9, (t3 - t2) * 1e-9, (t4 - t3) * 1e-9);

		// do something with the results
		if (dummy[0] != dummy[1] || dummy[0] != dummy[2] || dummy[0] != dummy[3])
			throw new AssertionError();
	}

	private static int testLambda(int v, IntSetter lambdaSetter, IntGetter lambdaGetter) {
		for (int i = 0; i < ITERATIONS; i++){
			lambdaSetter.set(v);
			v += (int)lambdaGetter.get();
		}
		return v;
	}

	private static int testReflection(int v, Field reflectedf) throws IllegalArgumentException, IllegalAccessException {
		for (int i = 0; i < ITERATIONS; i++){
			reflectedf.set(null, v);
			v += (int)reflectedf.get(null);
		}
			
		return v;
	}


	private static int testMH(int v, MethodHandle mhsetter, MethodHandle mhfgetter) throws Throwable {
		for (int i = 0; i < ITERATIONS; i++){
			mhsetter.invoke(v);
			v += (int)mhfgetter.invoke();
		}
		return v;
	}



	private static int testDirect(int v) {
		for (int i = 0; i < ITERATIONS; i++){
			f = v;
			v += f;
		}
		return v;
	}

}