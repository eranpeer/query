package org.query;

public interface EnumSetter {
	public void set(Object thiz, Enum<?> val);
}
