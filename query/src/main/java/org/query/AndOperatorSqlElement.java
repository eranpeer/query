package org.query;

import java.util.List;

final class AndOperatorSqlElement implements SqlElement {

	private SqlElement left;
	private SqlElement right;
	private SqlElement[] tail;

	AndOperatorSqlElement(SqlElement left, SqlElement right, SqlElement... tail) {
		this.left = left;
		this.right = right;
		this.tail = tail;
	}

	@Override
	public void apply(StringBuilder statementText, List<Object> queryParams) {
		statementText.append('(');
		left.apply(statementText, queryParams);
		statementText.append(')');
		statementText.append(" AND ");
		statementText.append('(');
		right.apply(statementText, queryParams);
		statementText.append(')');
		for (SqlElement e : tail) {
			statementText.append(" AND ");
			statementText.append('(');
			e.apply(statementText, queryParams);
			statementText.append(')');
		}
	}
}