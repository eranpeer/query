package org.query;

import java.util.List;

public interface StatementExecutor {
	void execute(String statementText, List<?> params);
}