package org.query;

import java.lang.reflect.Method;

public abstract class PropertyMetadata {
	private String columnLabel;
	private String columnName;
	private Object getterLambda;
	private Method getterMethod;
	private String propertyName;
	private Object setterLambda;
	private Method setterMethod;
	private boolean isGeneratedValue;
	private boolean isId;
	private String formattedColumnName;
	private String formattedFullColumnName;
	private boolean isLob;
	private DbEntityMetadata dbEntityMetadata;

	abstract public Object getValue(Object entity) throws Exception;

	abstract public void setValue(Object entity, DbValue row) throws Exception;

	abstract public void setValue(Object entity, Object value) throws Exception;

	public String getColumnLabel() {
		return columnLabel;
	}

	public void setColumnLabel(String columnLabel) {
		this.columnLabel = columnLabel;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getFormattedFullColumnName() {
		return formattedFullColumnName;
	}

	public void setFormatedFullColumnName(String formattedColumnName) {
		this.formattedFullColumnName = formattedColumnName;
	}

	public String getFormattedColumnName() {
		return formattedColumnName;
	}

	public Object getGetterLambda() {
		return getterLambda;
	}

	public void setGetterLambda(Object getterLambda) {
		this.getterLambda = getterLambda;
	}

	public Method getGetterMethod() {
		return getterMethod;
	}

	public void setGetterMethod(Method getterMethod) {
		this.getterMethod = getterMethod;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public Object getSetterLambda() {
		return setterLambda;
	}

	public void setSetterLambda(Object setterLambda) {
		this.setterLambda = setterLambda;
	}

	public Method getSetterMethod() {
		return setterMethod;
	}

	public void setSetterMethod(Method setterMethod) {
		this.setterMethod = setterMethod;
	}

	public boolean isGeneratedValue() {
		return isGeneratedValue;
	}

	public void setIsGeneratedValue(boolean isGeneratedValue) {
		this.isGeneratedValue = isGeneratedValue;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public void setIsId(boolean isId) {
		this.isId = isId;
	}

	public boolean isId() {
		return isId;
	}

	public void setFormattedColumnName(String formattedColumnName) {
		this.formattedColumnName = formattedColumnName;
	}

	public Object formatDbValue(Object javaValue) {
		return javaValue;
	}

	public boolean isLob() {
		return isLob;
	}

	public void setIsLob(boolean isLob) {
		this.isLob = isLob;
	}

	public void setDbEntityMetadata(DbEntityMetadata dbEntityMetadata) {
		this.dbEntityMetadata = dbEntityMetadata;
	}
	
	public DbEntityMetadata getDbEntityMetadata() {
		return dbEntityMetadata;
	}	
}