package org.query;

public interface WhereClauseRecorder2<T1, T2> {

	SqlElement record(T1 e1, T2 e2);

}