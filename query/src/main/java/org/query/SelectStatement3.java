package org.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectStatement3<T1, T2, T3> extends SelectStatementBase {

	public static class ExecutableStatement<T1, T2, T3> {

		private final JdbcQueryEngine engine;
		private final ParsedStatement parsedStatement;
		private PropertyMetadata[] selectedProperties;
		private DbEntityMetadata t1Metadata;
		private DbEntityMetadata t2Metadata;
		private DbEntityMetadata t3Metadata;

		public ExecutableStatement(JdbcQueryEngine engine, DbEntityMetadata t1Metadata, DbEntityMetadata t2Metadata, DbEntityMetadata t3Metadata,
				PropertyMetadata[] selectedProperties, ParsedStatement parsedStatement) {
			this.engine = engine;
			this.t1Metadata = t1Metadata;
			this.t2Metadata = t2Metadata;
			this.t3Metadata = t3Metadata;
			this.selectedProperties = selectedProperties;
			this.parsedStatement = parsedStatement;
		}

		public List<T1> findAll() {
			return engine.query(//
					parsedStatement.getStatementSql(), //
					parsedStatement.getArguments(), //
					rs -> collectAll(rs, t1Metadata, selectedProperties)//
			);
		}

		public T1 findOne() {
			return engine.query(//
					parsedStatement.getStatementSql(), //
					parsedStatement.getArguments(), //
					rs -> collectOne(rs, t1Metadata, selectedProperties)//
			);
		}

	}

	public interface TypedRowMapper<T1, T2,T3, Data> {
		Data map(T1 e1, T2 e2, T3 e3);
	}

	private final JdbcQueryEngine engine;
	private WhereClauseRecorder3<T1, T2, T3> filter;
	private DbEntityMetadata t1Metadata;
	private DbEntityMetadata t2Metadata;
	private DbEntityMetadata t3Metadata;

	public SelectStatement3(JdbcQueryEngine engine, Class<T1> t1, Class<T2> t2, Class<T3> t3) {
		this.engine = engine;
		this.t1Metadata = engine.getMetadata(t1);
		this.t2Metadata = engine.getMetadata(t2);
		this.t3Metadata = engine.getMetadata(t3);
	}

	public List<T1> findAll() {
		return makeExecutableStatement().findAll();
	}

	public <Data> List<Data> findAll(TypedRowMapper<T1, T2,T3, Data> rowMapper) {
		// return makeExecutableStatement().findAll(rowMapper);
		return null;
	}

	public ExecutableStatement<T1, T2, T3> makeExecutableStatement() {
		StringBuilder statementText = new StringBuilder();
		statementText.append("select ");
		final PropertyMetadata[] t1SelectedProperties = t1Metadata.getProperties();
		final PropertyMetadata[] t2SelectedProperties = t2Metadata.getProperties();
		final PropertyMetadata[] t3SelectedProperties = t3Metadata.getProperties();

		ArrayList<PropertyMetadata> allProperties = new ArrayList<>(
				t1SelectedProperties.length + t2SelectedProperties.length);
		allProperties.addAll(Arrays.asList(t1SelectedProperties));
		allProperties.addAll(Arrays.asList(t2SelectedProperties));
		allProperties.addAll(Arrays.asList(t3SelectedProperties));

		final PropertyMetadata[] selectedProperties = allProperties.toArray(new PropertyMetadata[allProperties.size()]);

		QueryUtils.appendSelectedColumns(statementText, selectedProperties);
		statementText.append(" from ").append(t1Metadata.getFormattedTableName());
		statementText.append(", ").append(t2Metadata.getFormattedTableName());
		statementText.append(", ").append(t3Metadata.getFormattedTableName());
		;
		ArrayList<Object> queryParams = new ArrayList<>();
		if (filter != null) {
			statementText.append("\nwhere ");
			SqlElement wc = WhereClauseUtils.recordWhereClause(filter, t1Metadata, t2Metadata, t3Metadata);
			wc.apply(statementText, queryParams);
		}
		ParsedStatement parsedStatement = new ParsedStatement(statementText.toString(), queryParams.toArray());
		return new ExecutableStatement<>(engine, t1Metadata, t2Metadata,t3Metadata, selectedProperties, parsedStatement);
	}

	public SelectStatement3<T1, T2, T3> where(WhereClauseRecorder3<T1, T2, T3> filter) {
		this.filter = filter;
		return this;
	}

	public T1 findOne() {
		return makeExecutableStatement().findOne();
	}
}