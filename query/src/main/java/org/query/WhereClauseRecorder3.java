package org.query;

public interface WhereClauseRecorder3<T1, T2, T3> {

	SqlElement record(T1 e1, T2 e2, T3 e3);

}