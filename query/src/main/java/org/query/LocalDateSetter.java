package org.query;

import java.time.LocalDate;

public interface LocalDateSetter {
	public void set(Object thiz, LocalDate val);
}