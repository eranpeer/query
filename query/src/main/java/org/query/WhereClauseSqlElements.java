package org.query;

import java.util.List;

import org.query.WhereClauseBuilder.PropertyClause;

public class WhereClauseSqlElements {

	private static abstract class SqlCompareElement implements SqlElement {
		protected final PropertyMetadata left;
		protected final Object expectedValue;

		public SqlCompareElement(PropertyMetadata left, Object right) {
			this.left = left;
			this.expectedValue = right;
		}

		public void apply(StringBuilder statementText, List<Object> queryParams) {
			boolean compare2columns = expectedValue instanceof PropertyClause;
			statementText.append('(').append(left.getFormattedFullColumnName()).append(getOperator())
					.append(formatRightValue(compare2columns)).append(')');
			if (!compare2columns)
				queryParams.add(left.formatDbValue(expectedValue));
		}

		protected String formatRightValue(boolean compare2columns) {
			return compare2columns ? ((PropertyClause) expectedValue).getProperty().getFormattedFullColumnName() : "?";
		}

		protected abstract Object getOperator();
	}

	static class In implements SqlElement {
		protected final PropertyMetadata left;
		protected final Object[] expectedValues;

		public In(PropertyMetadata left, Object... right) {
			this.left = left;
			this.expectedValues = right;
		}

		public void apply(StringBuilder statementText, List<Object> queryParams) {
			statementText.append('(').append(left.getFormattedFullColumnName()).append(" in (NULL,");
			for (Object object : expectedValues) {
				statementText.append('?').append(',');
				queryParams.add(object);
			}
			statementText.setLength(statementText.length() - 1);
			statementText.append(')');
			statementText.append(')');
		}
	}

	static class Eq extends WhereClauseSqlElements.SqlCompareElement {

		public Eq(PropertyMetadata left, Object right) {
			super(left, right);
		}

		protected String getOperator() {
			return "=";
		}
	}

	static class Lt extends WhereClauseSqlElements.SqlCompareElement {

		public Lt(PropertyMetadata left, Object right) {
			super(left, right);
		}

		protected String getOperator() {
			return "<";
		}
	}

	static class Gt extends WhereClauseSqlElements.SqlCompareElement {

		public Gt(PropertyMetadata left, Object right) {
			super(left, right);
		}

		protected String getOperator() {
			return ">";
		}
	}

	static class Le extends WhereClauseSqlElements.SqlCompareElement {

		public Le(PropertyMetadata left, Object right) {
			super(left, right);
		}

		protected String getOperator() {
			return "<=";
		}
	}

	static class Ge extends WhereClauseSqlElements.SqlCompareElement {

		public Ge(PropertyMetadata left, Object right) {
			super(left, right);
		}

		protected String getOperator() {
			return ">=";
		}
	}

}