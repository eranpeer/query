package org.query;

import java.util.ArrayList;
import java.util.List;

public class InsertStatement<T1> {

	public static class ExecutableStatement<T1> {

		private final JdbcQueryEngine engine;
		private final ParsedStatement parsedStatement;
		private final DbEntityMetadata t1Metadata;
		private final PropertyMetadata[] generatedProperties;

		public ExecutableStatement(JdbcQueryEngine engine, DbEntityMetadata t1Metadata,
				PropertyMetadata[] generatedProperties, ParsedStatement parsedStatement) {
			this.engine = engine;
			this.t1Metadata = t1Metadata;
			this.parsedStatement = parsedStatement;
			this.generatedProperties = generatedProperties;
		}

		public void insert(T1 value) {
			Object[] args = getArgs(value);
			engine.update(parsedStatement.getStatementSql(), //
					args, generatedProperties == null || generatedProperties.length == 0 ? null : rs -> {
						try {
							rs.next();
							QueryUtils.populate(value, new DbRowImpl(rs), generatedProperties);
						} catch (Exception e) {
							throw new RuntimeException(e);
						}
					});
		}

		public void insert(@SuppressWarnings("unchecked") T1... values) {
			for (int i = 0; i < values.length; i++) {
				insert(values[i]);
			}
		}

		private Object[] getArgs(T1 value) {
			final PropertyMetadata[] properties = t1Metadata.getProperties();
			ArrayList<Object> args = new ArrayList<>(properties.length);
			try {
				for (int i = 0; i < properties.length; i++) {
					if (shouldIncludeProperty(properties[i])) {
						Object val = properties[i].formatDbValue(properties[i].getValue(value));
						args.add(val);
					}
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			return args.toArray();
		}
	}

	private static boolean shouldIncludeProperty(PropertyMetadata property) {
		return !property.isGeneratedValue();
	}

	private final JdbcQueryEngine engine;
	private final DbEntityMetadata t1Metadata;

	public InsertStatement(JdbcQueryEngine engine, Class<T1> t1) {
		this.engine = engine;
		this.t1Metadata = engine.getMetadata(t1);
	}

	public ExecutableStatement<T1> makeExecutableStatement() {
		StringBuilder statementText = new StringBuilder();
		statementText.append("insert into ");
		statementText.append(t1Metadata.getFormattedTableName());
		statementText.append('(');
		QueryUtils.appendInsertedColumns(statementText, t1Metadata.getProperties());
		statementText.append(')');
		statementText.append(" values ");
		statementText.append('(');
		List<PropertyMetadata> generatedProperties = new ArrayList<>();
		for (int i = 0; i < t1Metadata.getProperties().length; i++) {
			if (shouldIncludeProperty(t1Metadata.getProperties()[i])) {
				statementText.append('?');
			} else {
				statementText.append("DEFAULT");
				generatedProperties.add(t1Metadata.getProperties()[i]);
			}
			statementText.append(',');
		}
		if (t1Metadata.getProperties().length > 0)
			statementText.setLength(statementText.length() - 1);

		statementText.append(')');
		ParsedStatement parsedStatement = new ParsedStatement(statementText.toString(), null);
		return new ExecutableStatement<>(engine, t1Metadata,
				generatedProperties.toArray(new PropertyMetadata[generatedProperties.size()]), parsedStatement);
	}

	public void value(T1 val) {
		makeExecutableStatement().insert(val);
	}

	@SuppressWarnings("unchecked")
	public void values(T1... values) {
		makeExecutableStatement().insert(values);
	}
}