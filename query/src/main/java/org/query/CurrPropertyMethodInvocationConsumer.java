package org.query;

import java.util.function.Consumer;

class CurrPropertyMethodInvocationConsumer implements Consumer<PropertyMethodInvocation> {

	private PropertyMethodInvocation lastCalledEntityMethod = null;

	public boolean methodInvocationSpecified() {
		return lastCalledEntityMethod != null;
	}

	public void accept(PropertyMethodInvocation method) {
		lastCalledEntityMethod = method;
	}

	public PropertyMethodInvocation takeLastMethodInvocation() {
		PropertyMethodInvocation method = lastCalledEntityMethod;
		lastCalledEntityMethod = null;
		return method;
	}

	protected <T> Object takeVal(T value) {
		Object val = value;
		if (methodInvocationSpecified()) {
			val = takePropertyMetadata();
		}
		return val;
	}

	protected PropertyMetadata takePropertyMetadata() {
		return takeLastMethodInvocation().getPropertyMetadata();
	}

}