package org.query;

public interface IntegerWrapperGetter {
	public Integer get(Object thiz);
}