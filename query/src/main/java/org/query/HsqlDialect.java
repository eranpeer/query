package org.query;

public class HsqlDialect implements SqlDialect {
	@Override
	public String formatColumnLabel(String tableName, String columnName) {
		return tableName.concat(".").concat(columnName);
	}

	@Override
	public String formatFullColumnName(String tableName, String columnName) {
		return '"' + tableName.concat("\".").concat("\"" + columnName + "\"");
	}
	
	@Override
	public String formatColumnName(String tableName, String columnName) {
		return '"' + columnName + '"';
	}

	@Override
	public String formatTableName(String tableName) {
		return '"' + tableName + '"';
	}

	
}