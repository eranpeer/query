package org.query;

import java.sql.Timestamp;

public interface TimestampGetter {
	public Timestamp get(Object thiz);
}