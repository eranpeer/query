package org.query;

import java.util.List;

public interface SqlElement {
	void apply(StringBuilder statementText, List<Object> queryParams);
}