package org.query;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

public class DeleteStatement<T1> {

	public static class ExecutableStatement<T1> {

		private final JdbcQueryEngine engine;
		private final ParsedStatement parsedStatement;

		public ExecutableStatement(JdbcQueryEngine engine, DbEntityMetadata t1Metadata,
				ParsedStatement parsedStatement) {
			this.engine = engine;
			this.parsedStatement = parsedStatement;
		}

		public int execute() {
			return engine.update(parsedStatement.getStatementSql(), //
					parsedStatement.getArguments(), //
					null);
		}
	}

	private final JdbcQueryEngine engine;
	private WhereClauseRecorder<T1> filter;
	private DbEntityMetadata t1Metadata;

	public DeleteStatement(JdbcQueryEngine engine, Class<T1> t1) {
		this.engine = engine;
		this.t1Metadata = engine.getMetadata(t1);
	}

	public int delete(T1... elements) {
		return delete(Arrays.asList(elements));
	}

	public int delete(List<T1> elements) {
		AbstractList<Object> ids = new AbstractList<Object>() {

			@Override
			public Object get(int index) {
				try {
					return t1Metadata.getIdProperty().getValue(elements.get(index));
				} catch (Exception e) {
					return null;
				}
			}

			@Override
			public int size() {
				return elements.size();
			}
		};

		where(e -> new WhereClauseSqlElements.In(t1Metadata.getIdProperty(), ids.toArray()));
		return makeExecutableStatement().execute();
	}

	@SuppressWarnings("unchecked")
	public ExecutableStatement<T1> makeExecutableStatement() {
		StringBuilder statementText = new StringBuilder();
		statementText.append("delete ");
		statementText.append(" from ");
		statementText.append(t1Metadata.getFormattedTableName());
		ArrayList<Object> queryParams = new ArrayList<>();
		if (filter != null) {
			statementText.append("\nwhere ");
			SqlElement wc = WhereClauseUtils.recordWhereClause(filter, t1Metadata);
			wc.apply(statementText, queryParams);
		}

		ParsedStatement parsedStatement = new ParsedStatement(statementText.toString(), queryParams.toArray());
		return new ExecutableStatement<>(engine, t1Metadata, parsedStatement);
	}

	public DeleteStatement<T1> where(WhereClauseRecorder<T1> filterRecorder) {
		this.filter = filterRecorder;
		return this;
	}

	public int execute() {
		int deleted = makeExecutableStatement().execute();
		return deleted;
	}
}