package org.query;

public interface SetClauseRecorder<T1> {
	void record(T1 t);
}