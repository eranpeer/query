package org.query;

import java.util.ArrayList;
import java.util.List;

public class SetClauseBuilder extends CurrPropertyMethodInvocationConsumer {

	public static final ThreadLocal<SetClauseBuilder> builder = ThreadLocal.<SetClauseBuilder> withInitial(() -> null);

	private static class PropertyUpdate {
		private final PropertyMetadata updatedProperty;
		private final Object newValue;

		public PropertyUpdate(PropertyMetadata updatedProperty, Object newValue) {
			this.updatedProperty = updatedProperty;
			this.newValue = newValue;
		}
	}

	List<PropertyUpdate> recordedChanges = new ArrayList<>();

	@Override
	public void accept(PropertyMethodInvocation method) {
		if (method.isGetter()) {
			super.accept(method);
			return;
		}
		Object argument = takeVal(method.getArgument());
		recordedChanges.add(new PropertyUpdate(method.getPropertyMetadata(), argument));
	}

	SqlElement getRecordedSetClouse() {
		return new SqlElement() {
			@Override
			public void apply(StringBuilder statementText, List<Object> queryParams) {
				for (PropertyUpdate update : recordedChanges) {
					statementText.append(update.updatedProperty.getFormattedFullColumnName()).append('=').append('?');
					statementText.append(',');
					queryParams.add(update.updatedProperty.formatDbValue(update.newValue));
				}
				if (recordedChanges.size() > 0)
					statementText.setLength(statementText.length() - 1);
			}
		};
	}
}