package org.query;

import org.query.WhereClauseSqlElements.Eq;
import org.query.WhereClauseSqlElements.Ge;
import org.query.WhereClauseSqlElements.Gt;
import org.query.WhereClauseSqlElements.In;
import org.query.WhereClauseSqlElements.Le;
import org.query.WhereClauseSqlElements.Lt;

public class WhereClauseBuilder extends CurrPropertyMethodInvocationConsumer {

	public static final ThreadLocal<WhereClauseBuilder> builder = ThreadLocal
			.<WhereClauseBuilder> withInitial(() -> null);

	public class PropertyClause<T> {

		private final PropertyMetadata property;

		public PropertyClause(PropertyMetadata propertyDetails) {
			this.property = propertyDetails;
		}


		public SqlElement Eq(T right) {
			Object val = takeVal(right);
			return new Eq(property, val);
		}

		public SqlElement Eq(PropertyClause<T> right) {
			return new Eq(property, right);
		}

		public SqlElement Le(T right) {
			Object val = takeVal(right);
			return new Le(property, val);
		}

		public SqlElement Lt(T right) {
			Object val = takeVal(right);
			return new Lt(property, val);
		}

		public SqlElement Gt(T right) {
			Object val = takeVal(right);
			return new Gt(property, val);
		}

		public SqlElement Ge(T right) {
			Object val = takeVal(right);
			return new Ge(property, val);
		}

		@SuppressWarnings("unchecked")
		public SqlElement In(T... values) {
			// Object val = takeVal(value);
			return new In(property, values);
		}
		
		public PropertyMetadata getProperty() {
			return property;
		}
	}

	public AndOperatorSqlElement and(SqlElement left, SqlElement right, SqlElement... tail) {
		return new AndOperatorSqlElement(left, right, tail);
	}

	public OrOperatorSqlElement or(SqlElement left, SqlElement right, SqlElement... tail) {
		return new OrOperatorSqlElement(left, right, tail);
	}

	public <T> PropertyClause<T> property() {
		if (!methodInvocationSpecified()) {
			throw new RuntimeException(
					"No property specified! Use a getter to specify the property. For example: propery(worker.getName())...");
		}
		return new PropertyClause<T>(takePropertyMetadata());
	}

}