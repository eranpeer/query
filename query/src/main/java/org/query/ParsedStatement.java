package org.query;

public class ParsedStatement {
	final private String sql;
	final private Object[] arguments;

	public ParsedStatement(String sql, Object[] arguments) {
		this.sql = sql;
		this.arguments = arguments;
	}

	public String getStatementSql() {
		return sql;
	}

	public Object[] getArguments() {
		return arguments;
	}
}