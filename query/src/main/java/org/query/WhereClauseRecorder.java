package org.query;

public interface WhereClauseRecorder<T1> {

	SqlElement record(T1 t);
}