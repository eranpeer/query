package org.query;

public class DbEntity {
	private final DbEntityMetadata metadata;
	private final Object[] values;

	public DbEntity(DbEntityMetadata metadata) {
		this.metadata = metadata;
		values = new Object[metadata.getProperties().length];
	}

	public DbEntityMetadata getMetadata() {
		return metadata;
	}

	public void setValue(String propertyName, Object value) {
		int index = getValueIndex(propertyName);
		values[index] = value;
	}

	public void setValue(int index, Object value) {
		values[index] = value;
	}

	private int getValueIndex(String propertyName) {
		final PropertyMetadata[] properties = metadata.getProperties();
		for (int i = 0; i < properties.length; i++) {
			if (properties[i].getPropertyName().equals(propertyName)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public String toString() {
		final PropertyMetadata[] properties = metadata.getProperties();
		StringBuilder buff = new StringBuilder();
		buff.append('[');
		for (int i = 0; i < properties.length; i++) {
			buff.append(properties[i].getPropertyName()).append('=').append(values[i]).append(',');
		}
		if (buff.charAt(buff.length() - 1) == ',')
			buff.setLength(buff.length() - 1);
		buff.append(']');
		return buff.toString();
	}
}