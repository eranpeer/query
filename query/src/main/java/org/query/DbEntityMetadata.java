package org.query;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.Blob;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.query.BeanMaker.HasCookie;

import net.bytebuddy.asm.Advice.Argument;
import net.bytebuddy.implementation.bind.annotation.Origin;
import net.bytebuddy.implementation.bind.annotation.RuntimeType;
import net.bytebuddy.implementation.bind.annotation.This;

public class DbEntityMetadata {

	private static class BytePropertyMetadata extends PropertyMetadata {

		@Override
		public Object getValue(Object entity) throws Exception {
			return ((ByteGetter) getGetterLambda()).get(entity);
		}

		@Override
		public void setValue(Object entity, DbValue row) throws Exception {
			((ByteSetter) getSetterLambda()).set(entity, row.asByte());
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((ByteSetter) getSetterLambda()).set(entity, (Byte) value);
		}
	}

	private static class DatePropertyMetadata extends PropertyMetadata {

		@Override
		public Object getValue(Object entity) throws Exception {
			return ((DateGetter) getGetterLambda()).get(entity);
		}

		@Override
		public void setValue(Object entity, DbValue row) throws Exception {
			((DateSetter) getSetterLambda()).set(entity, row.asDate());
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((DateSetter) getSetterLambda()).set(entity, (Date) value);
		}

	}


	private static class JavaDatePropertyMetadata extends PropertyMetadata {

		@Override
		public Object getValue(Object entity) throws Exception {
			return ((JavaDateGetter) getGetterLambda()).get(entity);
		}

		@Override
		public void setValue(Object entity, DbValue row) throws Exception {
			((JavaDateSetter) getSetterLambda()).set(entity, row.asJavaDate());
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((JavaDateSetter) getSetterLambda()).set(entity, (Date) value);
		}

	}

	public static class DispatchIterceptor implements MethodInvocationInterceptor {

		@RuntimeType
		public Object intercept(@This Object thiz, @Origin Method method) {
			HasCookie hasCookie = (HasCookie) thiz;
			ProxyCookie proxyCookie = (ProxyCookie) hasCookie.__cookie__();
			return proxyCookie.interceptor.intercept(thiz, method);
		}

		@Override
		public void intercept(@This Object thiz, @Origin Method method, @RuntimeType @Argument(1) Object argument) {
			HasCookie hasCookie = (HasCookie) thiz;
			ProxyCookie proxyCookie = (ProxyCookie) hasCookie.__cookie__();
			proxyCookie.interceptor.intercept(thiz, method, argument);
		}
	}

	private static class DoublePropertyMetadata extends PropertyMetadata {

		@Override
		public Object getValue(Object entity) throws Exception {
			return ((DoubleGetter) getGetterLambda()).get(entity);
		}

		@Override
		public void setValue(Object entity, DbValue row) throws Exception {
			((DoubleSetter) getSetterLambda()).set(entity, row.asDouble());
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((DoubleSetter) getSetterLambda()).set(entity, (Double) value);
		}
	}

	private static class FloatPropertyMetadata extends PropertyMetadata {

		@Override
		public Object getValue(Object entity) throws Exception {
			return ((FloatGetter) getGetterLambda()).get(entity);
		}

		@Override
		public void setValue(Object entity, DbValue row) throws Exception {
			((FloatSetter) getSetterLambda()).set(entity, row.asFloat());
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((FloatSetter) getSetterLambda()).set(entity, (Float) value);
		}

	}

	private static class IntegerPropertyMetadata extends PropertyMetadata {

		@Override
		public Object getValue(Object entity) throws Exception {
			return ((IntegerWrapperGetter) getGetterLambda()).get(entity);
		}

		@Override
		public void setValue(Object entity, DbValue row) throws Exception {
			((IntegerWrapperSetter) getSetterLambda()).set(entity, row.asIntWrapper());
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((IntegerWrapperSetter) getSetterLambda()).set(entity, (Integer) value);
		}

		@Override
		public String toString() {
			return super.toString();
		}
	}

	private static class LongWrapperPropertyMetadata extends PropertyMetadata {

		@Override
		public Object getValue(Object entity) throws Exception {
			return ((LongWrapperGetter) getGetterLambda()).get(entity);
		}

		@Override
		public void setValue(Object entity, DbValue row) throws Exception {
			((LongWrapperSetter) getSetterLambda()).set(entity, row.asLongWrapper());
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((LongWrapperSetter) getSetterLambda()).set(entity, (Long) value);
		}

		@Override
		public String toString() {
			return super.toString();
		}
	}

	private static class IntPropertyMetadata extends PropertyMetadata {

		@Override
		public Object getValue(Object entity) throws Exception {
			return ((IntGetter) getGetterLambda()).get(entity);
		}

		@Override
		public void setValue(Object entity, DbValue row) throws Exception {
			((IntSetter) getSetterLambda()).set(entity, row.asInt());
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((IntSetter) getSetterLambda()).set(entity, (int) value);
		}
	}

	private static class LocalDatePropertyMetadata extends PropertyMetadata {

		@Override
		public Object getValue(Object entity) throws Exception {
			return ((LocalDateGetter) getGetterLambda()).get(entity);
		}

		@Override
		public void setValue(Object entity, DbValue row) throws Exception {
			((LocalDateSetter) getSetterLambda()).set(entity, row.asLocalDate());
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((LocalDateSetter) getSetterLambda()).set(entity, (LocalDate) value);
		}

	}

	private static class LongPropertyMetadata extends PropertyMetadata {

		@Override
		public Object getValue(Object entity) throws Exception {
			return ((LongGetter) getGetterLambda()).get(entity);
		}

		@Override
		public void setValue(Object entity, DbValue row) throws Exception {
			((LongSetter) getSetterLambda()).set(entity, row.asLong());
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((LongSetter) getSetterLambda()).set(entity, (Long) value);
		}
	}

	private static class PropertyBuilder {
		public String propertyName;
		public Method setterMethod;
		public Method getterMethod;

		private Column getColumnAnnotation() {
			final Column getterColumnAnnotation = getterMethod.getAnnotation(Column.class);
			final Column setterColumnAnnotation = setterMethod.getAnnotation(Column.class);
			Column columnAnnotation = getterColumnAnnotation;
			if (columnAnnotation == null)
				columnAnnotation = setterColumnAnnotation;
			return columnAnnotation;
		}

		private String getColumnName(Column columnAnnotation, String propertyName) {
			String columnName = null;
			if (columnAnnotation != null)
				columnName = columnAnnotation.name();
			else
				columnName = propertyName;
			return columnName;
		}

		private boolean getIsGeneratedValue() {
			boolean getterIsGeneratedValue = getterMethod.isAnnotationPresent(GeneratedValue.class);
			boolean setterIsGeneratedValue = setterMethod.isAnnotationPresent(GeneratedValue.class);
			boolean isGeneratedValue = getterIsGeneratedValue;
			isGeneratedValue |= setterIsGeneratedValue;
			return isGeneratedValue;
		}

		private boolean getIsId() {
			boolean getterIsId = getterMethod.isAnnotationPresent(Id.class);
			boolean setterIsId = setterMethod.isAnnotationPresent(Id.class);
			boolean isId = getterIsId;
			isId |= setterIsId;
			return isId;
		}

		private boolean getIsLob() {
			boolean getterIsLob = getterMethod.isAnnotationPresent(Lob.class);
			boolean setterIsLob = setterMethod.isAnnotationPresent(Lob.class);
			boolean isLob = getterIsLob;
			isLob |= setterIsLob;
			return isLob;
		}

		private Class<?> getPropertyType() {
			final Class<?> getterPropertyType = getterMethod.getReturnType();
			final Class<?> setterPropertyType = setterMethod.getParameterTypes()[0];
			if (setterPropertyType != getterPropertyType)
				throw new RuntimeException(
						"Property:" + propertyName + ", property type mismatch. getter and setter do not match");
			Class<?> propertyType = getterPropertyType;
			return propertyType;
		}

		PropertyMetadata make(DbEntityMetadata dbEntityMetadataa, String tableName, SqlDialect dialect) {
			Column columnAnnotation = getColumnAnnotation();
			boolean isGeneratedValue = getIsGeneratedValue();
			boolean isId = getIsId();
			boolean isLob = getIsLob();
			Class<?> propertyType = getPropertyType();
			final Class<?> setterLambdaType = getSetterLambdaType(propertyType);
			final Class<?> getterLambdaType = getGetterLambdaType(propertyType);
			String columnName = getColumnName(columnAnnotation, propertyName);
			String fullColumnName = dialect.formatFullColumnName(tableName, columnName);
			String columnLabel = dialect.formatColumnLabel(tableName, propertyName);
			PropertyMetadata property = createPropertyMetadata(propertyType);

			property.setColumnName(columnName);
			property.setFormattedColumnName(dialect.formatColumnName(tableName, columnName));
			property.setFormatedFullColumnName(fullColumnName);
			property.setColumnLabel(columnLabel);
			property.setIsGeneratedValue(isGeneratedValue);
			property.setIsId(isId);
			property.setIsLob(isLob);
			property.setGetterMethod(getterMethod);
			property.setSetterMethod(setterMethod);
			try {
				property.setSetterLambda(BeanMaker.makeSetter(setterMethod, setterLambdaType));
				property.setGetterLambda(BeanMaker.makeGetter(getterMethod, getterLambdaType));
			} catch (Throwable e) {
				throw new RuntimeException(e);
			}

			return property;
		}

		// private Transient getTransientAnnotation() {
		// final Transient getterAnnotation =
		// getterMethod.getAnnotation(Transient.class);
		// final Transient setterAnnotation =
		// setterMethod.getAnnotation(Transient.class);
		// Transient annotation = getterAnnotation;
		// if (annotation == null)
		// annotation = setterAnnotation;
		// return annotation;
		// }
	}

	private static class ProxyCookie {
		final MethodInvocationInterceptor interceptor;
		final DbEntityMetadata metadata;

		public ProxyCookie(MethodInvocationInterceptor interceptor, DbEntityMetadata metadata) {
			super();
			this.interceptor = interceptor;
			this.metadata = metadata;
		}
	}

	private static class ShortPropertyMetadata extends PropertyMetadata {

		@Override
		public Object getValue(Object entity) throws Exception {
			return ((ShortGetter) getGetterLambda()).get(entity);
		}

		@Override
		public void setValue(Object entity, DbValue row) throws Exception {
			((ShortSetter) getSetterLambda()).set(entity, row.asShort());
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((ShortSetter) getSetterLambda()).set(entity, (Short) value);
		}
	}

	private static class StringPropertyMetadata extends PropertyMetadata {

		@Override
		public Object getValue(Object entity) throws Exception {
			return ((StringGetter) getGetterLambda()).get(entity);
		}

		@Override
		public void setValue(Object entity, DbValue row) throws Exception {
			((StringSetter) getSetterLambda()).set(entity, row.asString());
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((StringSetter) getSetterLambda()).set(entity, (String) value);
		}

		@Override
		public String toString() {
			return super.toString();
		}
	}

	private static class EnumPropertyMetadata extends PropertyMetadata {

		final Class<? extends Enum> propertyType;

		public EnumPropertyMetadata(Class<? extends Enum> propertyType) {
			super();
			this.propertyType = propertyType;
		}

		@Override
		public Object getValue(Object entity) throws Exception {
			Object getterLambda = getGetterLambda();
			EnumGetter enumGetter = (EnumGetter) getterLambda;
			return enumGetter.get(entity);
		}

		@Override
		@SuppressWarnings("unchecked")
		public void setValue(Object entity, DbValue row) throws Exception {
			String enumStr = row.asString();
			Enum<?> enumValue = null;
			if (enumStr != null)
				enumValue = Enum.valueOf(propertyType, enumStr);
			((EnumSetter) getSetterLambda()).set(entity, enumValue);
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((EnumSetter) getSetterLambda()).set(entity, (Enum) value);
		}

		public Object formatDbValue(Object value) {
			try {
				if (value == null)
					return null;
				return ((Enum<?>) value).name();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

	}

	private static class ByteArrayPropertyMetadata extends PropertyMetadata {

		@Override
		public Object getValue(Object entity) throws Exception {
			return ((ByteArrayGetter) getGetterLambda()).get(entity);
		}

		@Override
		public void setValue(Object entity, DbValue row) throws Exception {
			byte[] bytes = null;
			if (isLob()) {
				Blob blob = row.asBlob();
				if (blob != null)
					bytes = blob.getBytes(1, (int) blob.length());
			} else {
				bytes = row.asBytes();
			}
			((ByteArraySetter) getSetterLambda()).set(entity, bytes);
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((ByteArraySetter) getSetterLambda()).set(entity, (byte[]) value);
		}

		public Object formatDbValue(Object value) {
			if (!isLob())
				return value;
			final byte[] byteArray = (byte[]) value;
			return new Blob() {

				@Override
				public void truncate(long len) throws SQLException {
					throw new UnsupportedOperationException();
				}

				@Override
				public int setBytes(long pos, byte[] bytes, int offset, int len) throws SQLException {
					throw new UnsupportedOperationException();
				}

				@Override
				public int setBytes(long pos, byte[] bytes) throws SQLException {
					throw new UnsupportedOperationException();
				}

				@Override
				public OutputStream setBinaryStream(long pos) throws SQLException {
					throw new UnsupportedOperationException();
				}

				@Override
				public long position(Blob pattern, long start) throws SQLException {
					throw new UnsupportedOperationException();
				}

				@Override
				public long position(byte[] pattern, long start) throws SQLException {
					throw new UnsupportedOperationException();
				}

				@Override
				public long length() throws SQLException {
					return byteArray.length;
				}

				@Override
				public byte[] getBytes(long pos, int length) throws SQLException {
					if (length == byteArray.length)
						return byteArray;
					return Arrays.copyOfRange(byteArray, (int)pos -1, (int)pos + length -1);
				}

				@Override
				public InputStream getBinaryStream(long pos, long length) throws SQLException {
					return new ByteArrayInputStream(byteArray, (int) (pos - 1), (int) length);
				}

				@Override
				public InputStream getBinaryStream() throws SQLException {
					return new ByteArrayInputStream(byteArray);
				}

				@Override
				public void free() throws SQLException {
				}
			};
		}

	}

	private static class TimePropertyMetadata extends PropertyMetadata {

		@Override
		public Object getValue(Object entity) throws Exception {
			return ((TimeGetter) getGetterLambda()).get(entity);
		}

		@Override
		public void setValue(Object entity, DbValue row) throws Exception {
			((TimeSetter) getSetterLambda()).set(entity, row.asTime());
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((TimeSetter) getSetterLambda()).set(entity, (Time) value);
		}

	}

	private static class TimestampPropertyMetadata extends PropertyMetadata {

		@Override
		public Object getValue(Object entity) throws Exception {
			return ((TimestampGetter) getGetterLambda()).get(entity);
		}

		@Override
		public void setValue(Object entity, DbValue row) throws Exception {
			((TimestampSetter) getSetterLambda()).set(entity, row.asTimestamp());
		}

		@Override
		public void setValue(Object entity, Object value) throws Exception {
			((TimestampSetter) getSetterLambda()).set(entity, (Timestamp) value);
		}

	}

	private static final Map<Class<?>, Class<?>> gettersMap = makeGetterMap();

	private static final Map<Class<?>, Class<?>> settersMap = makeSetterMap();

	private static Map<Class<?>, DbEntityMetadata> matadataTable =new ConcurrentHashMap<>();

	private static String getTableName(Class<?> dbEntityType) {
		final Table annotation = dbEntityType.getAnnotation(Table.class);
		if (annotation != null)
			return annotation.name();
		return dbEntityType.getSimpleName().toLowerCase();
	}

	private static void populdateProperties(DbEntityMetadata dbEntityMetadata) throws Throwable {
		Collection<PropertyBuilder> builders = makePropertyBuilders(dbEntityMetadata.dbEntityType);
		String tableName = getTableName(dbEntityMetadata.dbEntityType);
		PropertyMetadata[] properties = makeProperties(dbEntityMetadata, tableName, builders, dbEntityMetadata.dialect);
		dbEntityMetadata.setProperties(properties);
	}

	protected static Collection<PropertyBuilder> makePropertyBuilders(Class<?> dbEntityType) {
		LinkedHashMap<String, PropertyBuilder> propertyBuiders = new LinkedHashMap<>();
		Method[] methods = dbEntityType.getMethods();
		for (int i = 0; i < methods.length; i++) {
			Method method = methods[i];
			if (method.getDeclaringClass() == Object.class)
				continue;
			if (isObjectMethod(method))
				continue;
			if (Modifier.isStatic(method.getModifiers()))
				continue;
			if (method.isDefault())
				continue;
			if (method.getAnnotation(Transient.class) != null)
				continue;

			if (method.getReturnType().equals(void.class)) {
				if (method.getParameterCount() == 1) {
					String name = QueryUtils.getPropertyNameFromSetter(method);
					PropertyBuilder builder = getPropertyBuilder(propertyBuiders, name);
					builder.setterMethod = method;
				}
				continue;
			} else {
				if (method.getParameterCount() == 0) {
					String name = QueryUtils.getPropertyNameFromGetter(method);
					PropertyBuilder builder = getPropertyBuilder(propertyBuiders, name);
					builder.getterMethod = method;
				}
			}
		}

		Collection<PropertyBuilder> builders = propertyBuiders.values();
		return builders;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static PropertyMetadata createPropertyMetadata(Class<?> propertyType) {
		if (propertyType.equals(short.class))
			return new ShortPropertyMetadata();
		if (propertyType.equals(byte.class))
			return new BytePropertyMetadata();
		if (propertyType.equals(int.class))
			return new IntPropertyMetadata();
		if (propertyType.equals(long.class))
			return new LongPropertyMetadata();
		if (propertyType.equals(double.class))
			return new DoublePropertyMetadata();
		if (propertyType.equals(float.class))
			return new FloatPropertyMetadata();
		if (propertyType.equals(Integer.class))
			return new IntegerPropertyMetadata();
		if (propertyType.equals(Long.class))
			return new LongWrapperPropertyMetadata();
		if (propertyType.equals(String.class))
			return new StringPropertyMetadata();
		if (propertyType.equals(Timestamp.class))
			return new TimestampPropertyMetadata();
		if (propertyType.equals(java.sql.Date.class))
			return new DatePropertyMetadata();
		if (propertyType.equals(java.util.Date.class))
			return new JavaDatePropertyMetadata();
		if (propertyType.equals(java.sql.Time.class))
			return new TimePropertyMetadata();
		if (propertyType.equals(LocalDate.class))
			return new LocalDatePropertyMetadata();
		if (propertyType.isEnum())
			return new EnumPropertyMetadata((Class<? extends Enum>) propertyType);
		if (propertyType.equals(byte[].class))
			return new ByteArrayPropertyMetadata();
		return null;
	}

	public static DbEntityMetadata getDbEntityMetadata(Object dbEntity) {
		HasCookie hasCookie = (HasCookie) dbEntity;
		ProxyCookie proxyCookie = (ProxyCookie) hasCookie.__cookie__();
		return proxyCookie.metadata;
	}

	public static Class<?> getGetterLambdaType(Class<?> propertyType) {
		if (propertyType.isEnum())
			return EnumGetter.class;
		return gettersMap.get(propertyType);
	}

	private static PropertyBuilder getPropertyBuilder(LinkedHashMap<String, PropertyBuilder> propertiesMap,
			String propertyName) {
		PropertyBuilder property = propertiesMap.get(propertyName);
		if (property == null) {
			property = new PropertyBuilder();
			property.propertyName = propertyName;
			propertiesMap.put(propertyName, property);
		}
		return property;
	}

	public static Class<?> getSetterLambdaType(Class<?> propertyType) {
		if (propertyType.isEnum())
			return EnumSetter.class;
		return settersMap.get(propertyType);
	}

	private static boolean isObjectMethod(Method method) {
		try {
			Object.class.getDeclaredMethod(method.getName(), method.getParameterTypes());
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	public static DbEntityMetadata make(Class<?> dbEntityType, SqlDialect sqlDialect) {
		return matadataTable.computeIfAbsent(dbEntityType, k -> makeMatadata(k, sqlDialect));
	}

	private static DbEntityMetadata makeMatadata(Class<?> dbEntityType, SqlDialect sqlDialect) {
		try {
			DbEntityMetadata dbEntityMetadata = new DbEntityMetadata(dbEntityType, sqlDialect);
			populdateProperties(dbEntityMetadata);
			return dbEntityMetadata;
			
		} catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}

	public static Class<?> makeDynamicProxyType(final Class<?> entityType) {
		return BeanMaker.makeDynamicProxyClass(entityType, new DispatchIterceptor(), HasCookie.class);
	}

	private static Map<Class<?>, Class<?>> makeGetterMap() {
		Map<Class<?>, Class<?>> map = new HashMap<Class<?>, Class<?>>();
		map.put(boolean.class, BooleanGetter.class);
		map.put(byte.class, ByteGetter.class);
		map.put(char.class, CharGetter.class);
		map.put(double.class, DoubleGetter.class);
		map.put(float.class, FloatGetter.class);
		map.put(int.class, IntGetter.class);
		map.put(long.class, LongGetter.class);
		map.put(short.class, ShortGetter.class);
		map.put(Integer.class, IntegerWrapperGetter.class);
		map.put(Long.class, LongWrapperGetter.class);
		map.put(String.class, StringGetter.class);
		map.put(Timestamp.class, TimestampGetter.class);
		map.put(Time.class, TimeGetter.class);
		map.put(Date.class, DateGetter.class);
		map.put(java.util.Date.class, JavaDateGetter.class);
		map.put(byte[].class, ByteArrayGetter.class);
		return map;
	}

	protected static PropertyMetadata[] makeProperties(DbEntityMetadata dbEntityMetadata, String tableName,
			Collection<PropertyBuilder> values, SqlDialect dialect) {
		PropertyMetadata[] result = new PropertyMetadata[values.size()];
		int i = 0;
		for (PropertyBuilder propertyBuilder : values) {
			result[i] = propertyBuilder.make(dbEntityMetadata, tableName, dialect);
			i++;
		}
		return result;
	}

	private void setProperties(PropertyMetadata[] result) {
		this.properties = result;
		initProperties(dbEntityType, properties);
	}

	private static Map<Class<?>, Class<?>> makeSetterMap() {
		Map<Class<?>, Class<?>> map = new HashMap<Class<?>, Class<?>>();
		map.put(boolean.class, BooleanSetter.class);
		map.put(byte.class, ByteSetter.class);
		map.put(char.class, CharSetter.class);
		map.put(double.class, DoubleSetter.class);
		map.put(float.class, FloatSetter.class);
		map.put(int.class, IntSetter.class);
		map.put(long.class, LongSetter.class);
		map.put(short.class, ShortSetter.class);
		map.put(Integer.class, IntegerWrapperSetter.class);
		map.put(Long.class, LongWrapperSetter.class);
		map.put(String.class, StringSetter.class);
		map.put(Timestamp.class, TimestampSetter.class);
		map.put(Time.class, TimeSetter.class);
		map.put(Date.class, DateSetter.class);
		map.put(java.util.Date.class, JavaDateSetter.class);
		map.put(byte[].class, ByteArraySetter.class);
		return map;
	}

	private final Class<?> dbEntityType;

	private Class<?> dynamicProxyType;

	private PropertyMetadata[] properties;

	private PropertyMetadata idProperty;

	private final SqlDialect dialect;

	public DbEntityMetadata(Class<?> dbEntityType, SqlDialect sqlDialect) {
		this.dbEntityType = dbEntityType;
		this.dialect = sqlDialect;
		this.dynamicProxyType = makeDynamicProxyType(dbEntityType);
	}

	public Class<?> getDynamicProxyType() {
		return dynamicProxyType;
	}

	public Class<?> getEntityType() {
		return dbEntityType;
	}

	public String getFormattedTableName() {
		return this.dialect.formatTableName(getTableName(dbEntityType));
	}

	public PropertyMetadata getIdProperty() {
		return idProperty;
	}

	public PropertyMetadata[] getProperties() {
		return properties;
	}

	public PropertyMetadata getPropertyMetadata(Method method) {
		for (PropertyMetadata propertyMetadata : properties) {
			if (propertyMetadata.getGetterMethod().equals(method))
				return propertyMetadata;
		}
		for (PropertyMetadata propertyMetadata : properties) {
			if (propertyMetadata.getSetterMethod().equals(method))
				return propertyMetadata;
		}
		return null;
	}

	public PropertyMetadata getPropertyMetadata(String columnLabel) {
		for (PropertyMetadata propertyMetadata : properties) {
			if (propertyMetadata.getColumnLabel().equals(columnLabel))
				return propertyMetadata;
		}
		return null;
	}

	private <T> T instantiate(final Class<? extends T> proxyType, final MethodInvocationInterceptor interceptor)
			throws InstantiationException, IllegalAccessException {
		final T newInstance = proxyType.newInstance();
		((HasCookie) newInstance).__cookie__(new ProxyCookie(interceptor, this));
		return newInstance;
	}

	/**
	 * Create a proxy of type T that dispatches all invocations to interceptor.
	 * 
	 * @param interceptor
	 * @return
	 */
	public <T> T makeProxy(final MethodInvocationInterceptor interceptor) {
		try {
			@SuppressWarnings("unchecked")
			final Class<? extends T> proxyType = (Class<? extends T>) getDynamicProxyType();
			return instantiate(proxyType, interceptor);
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	private void initProperties(Class<?> dbEntityType, PropertyMetadata[] properties) {
		for (PropertyMetadata metadata : properties) {
			if (metadata.isId()) {
				if (idProperty != null) {
					throw new IllegalArgumentException("duplicate Id property in class " + dbEntityType.getName());
				}
				idProperty = metadata;
			}
			metadata.setDbEntityMetadata(this);
		}
	}
}