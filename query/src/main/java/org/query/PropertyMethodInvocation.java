package org.query;

import java.lang.reflect.Method;

public class PropertyMethodInvocation {
	private static Object argumentNotSet = new Object();
	private final Method method;
	private final Object argument;
	private final Object entity;

	public PropertyMethodInvocation(Object entity, Method method, Object argument) {
		this.entity = entity;
		this.method = method;
		this.argument = argument;
	}

	public PropertyMethodInvocation(Object entity, Method method) {
		this.entity = entity;
		this.method = method;
		this.argument = argumentNotSet;
	}

	public Object getArgument() {
		return argument;
	}

	public Method getMethod() {
		return method;
	}

	public boolean isSetter() {
		return !isGetter();
	}

	public boolean isGetter() {
		return argument == argumentNotSet;
	}

	public Object getEntity() {
		return entity;
	}

	public  PropertyMetadata getPropertyMetadata() {
		return getEntityMetadata().getPropertyMetadata(getMethod());
	}

	public  DbEntityMetadata getEntityMetadata() {
		return DbEntityMetadata.getDbEntityMetadata(getEntity());
	}

}