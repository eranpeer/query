package org.query;

import org.query.WhereClauseBuilder.PropertyClause;

public class Operators {

	public static <T> PropertyClause<T> property(T o) {
		WhereClauseBuilder wcb = WhereClauseBuilder.builder.get();
		ensureInsideWhereClause(wcb);
		return wcb.property();
	}

	public static <T> SqlElement and(SqlElement left, SqlElement right, SqlElement... tail) {
		WhereClauseBuilder wcb = WhereClauseBuilder.builder.get();
		ensureInsideWhereClause(wcb);
		return wcb.and(left, right, tail);
	}

	public static <T> SqlElement or(SqlElement left, SqlElement right, SqlElement... tail) {
		WhereClauseBuilder wcb = WhereClauseBuilder.builder.get();
		ensureInsideWhereClause(wcb);
		return wcb.or(left, right, tail);
	}

	private static void ensureInsideWhereClause(WhereClauseBuilder wcb) {
		if (wcb == null)
			throw new RuntimeException("method must be used inside a where clause");
	}
}