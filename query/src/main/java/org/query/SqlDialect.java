package org.query;

public interface SqlDialect {

	String formatTableName(String tableName);
	
	String formatColumnLabel(String tableName, String columnName);

	String formatFullColumnName(String tableName, String columnName);

	String formatColumnName(String tableName, String columnName);

}