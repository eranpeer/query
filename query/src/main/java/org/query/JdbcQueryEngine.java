package org.query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public interface JdbcQueryEngine {

	public static interface PreparedStatementProducer {
		public PreparedStatement produce(Connection con) throws SQLException;
	}

	public static interface PreparedStatementConsumer<T> {
		public T consume(PreparedStatement ps) throws SQLException;
	}

	public static interface ResultSetConsumer<T> {
		T consume(ResultSet rs) throws SQLException;
	}

	public static interface GeneratedValuesConsumer {
		void consume(ResultSet generatedValues) throws SQLException;
	}

	default <T> T query(String sql, Object[] args, ResultSetConsumer<T> callback) {
		PreparedStatementProducer psp = new PreparedStatementProducer() {

			@Override
			public PreparedStatement produce(Connection con) throws SQLException {
				PreparedStatement ps = con.prepareStatement(sql);
				for (int i = 1; i <= args.length; i++)
					ps.setObject(i, args[i - 1]);
				return ps;
			}
		};

		PreparedStatementConsumer<T> psc = new PreparedStatementConsumer<T>() {

			@Override
			public T consume(PreparedStatement ps) throws SQLException {
				try (ResultSet rs = ps.executeQuery()) {
					return callback.consume(rs);
				}
			}
		};

		return execute(psp, psc);
	}

	default int update(String sql, Object[] args, GeneratedValuesConsumer callback) {

		PreparedStatementProducer psp = new PreparedStatementProducer() {

			@Override
			public PreparedStatement produce(Connection con) throws SQLException {
				PreparedStatement ps = con.prepareStatement(sql,
						callback == null ? Statement.NO_GENERATED_KEYS : Statement.RETURN_GENERATED_KEYS);
				populateColumns(ps, args);
				return ps;
			}

			private void populateColumns(PreparedStatement ps, Object[] args) throws SQLException {
				for (int i = 1; i <= args.length; i++){
					ps.setObject(i, args[i - 1]);
				}
			}
		};

		PreparedStatementConsumer<Integer> psc = new PreparedStatementConsumer<Integer>() {
			@Override
			public Integer consume(PreparedStatement ps) throws SQLException {
				int count = ps.executeUpdate();
				if (callback != null) {
					try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
						callback.consume(generatedKeys);
					}
				}
				return count;
			}
		};

		return execute(psp, psc);
	}

	<T> T execute(PreparedStatementProducer psp, PreparedStatementConsumer<T> psc);

	DbEntityMetadata getMetadata(Class<?> t1);
}