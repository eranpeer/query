package org.query;

import java.util.List;

public class QueryOperations {

	private final JdbcQueryEngine engine;

	public class Scope1<T1> {

		private final Class<T1> t1;

		public Scope1(Class<T1> t1) {
			this.t1 = t1;
		}

		public SelectStatement1<T1> query() {
			return new SelectStatement1<>(engine, t1);
		}

		public InsertStatement<T1> insert() {
			return new InsertStatement<T1>(engine, t1);
		}

		public DeleteStatement<T1> delete() {
			return new DeleteStatement<T1>(engine, t1);
		}

		public UpdateStatement<T1> update() {
			return new UpdateStatement<T1>(engine, t1);
		}
	}

	public <T> Scope1<T> scope(Class<T> clazz) {
		return new Scope1<>(clazz);
	}

	public QueryOperations(JdbcQueryEngine engine) {
		this.engine = engine;
	}

	public <T1> SelectStatement1<T1> query(Class<T1> t1) {
		return scope(t1).query();
	}

	public <T1, T2> SelectStatement2<T1, T2> query(Class<T1> t1, Class<T2> t2) {
		return new SelectStatement2<T1, T2>(this.engine, t1, t2);
	}

	public <T1, T2, T3> SelectStatement3<T1, T2, T3> query(Class<T1> t1, Class<T2> t2, Class<T3> t3) {
		return new SelectStatement3<T1, T2, T3>(this.engine, t1, t2, t3);
	}
	
	public <T1> DeleteStatement<T1> delete(Class<T1> t1) {
		return scope(t1).delete();
	}

	@SuppressWarnings("unchecked")
	public <T> void insert(T val) {
		insert((Class<T>) val.getClass()).value(val);
	}

	@SuppressWarnings("unchecked")
	public <T> void delete(T... vals) {
		new DeleteStatement<T>(engine, (Class<T>) vals.getClass().getComponentType()).delete(vals);
	}

	@SuppressWarnings("unchecked")
	public <T> void delete(List<T> vals) {
		new DeleteStatement<T>(engine, (Class<T>) vals.get(0).getClass()).delete(vals);
	}

	public <T> InsertStatement<T> insert(final Class<T> t1) {
		return scope(t1).insert();
	}

	@SuppressWarnings("unchecked")
	public <T> boolean update(T val) {
		final int updated = new UpdateStatement<T>(engine, (Class<T>) val.getClass()).update(val);
		return updated == 1;
	}

	public <T1> UpdateStatement<T1> update(Class<T1> t1) {
		return scope(t1).update();
	}

}