package org.query;

import java.lang.invoke.CallSite;
import java.lang.invoke.LambdaMetafactory;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.modifier.Ownership;
import net.bytebuddy.description.modifier.Visibility;
import net.bytebuddy.dynamic.DynamicType.Builder;
import net.bytebuddy.dynamic.DynamicType.Unloaded;
import net.bytebuddy.dynamic.loading.ClassLoadingStrategy;
import net.bytebuddy.implementation.FieldAccessor;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.matcher.ElementMatchers;

public class BeanMaker {

	private static class PropertyDetails {
		public final String name;
		public Method setter;
		public Method getter;

		public PropertyDetails(String name) {
			this.name = name;
		}

		public void setGetter(Method getter) {
			if (setter != null) {
				if (!getter.getReturnType().equals(setter.getParameterTypes()[0])) {
					throw new RuntimeException("getter setter mismatch");
				}
			}
			this.getter = getter;
		}

		public void setSetter(Method setter) {
			if (getter != null) {
				if (!getter.getReturnType().equals(setter.getParameterTypes()[0])) {
					throw new RuntimeException("getter setter mismatch");
				}
			}
			this.setter = setter;
		}

		public Class<?> getType() {
			if (getter != null) {
				return getter.getReturnType();
			}
			return setter.getParameterTypes()[0];
		}
	}

	public static <T> Class<? extends T> makeBeanClass(Class<T> beanInterface) {
		Collection<BeanMaker.PropertyDetails> properties = calcProperties(beanInterface);
		return implementInteface(beanInterface, properties);
	}

	public static <T> T makeBeanFromInterface(Class<T> beanInterface) {
		Collection<BeanMaker.PropertyDetails> properties = calcProperties(beanInterface);
		try {
			return implementInteface(beanInterface, properties).newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	public static <T> T makeBeanFromClass(Class<T> beanInterface) {
		Collection<BeanMaker.PropertyDetails> properties = calcProperties(beanInterface);
		try {
			return implementInteface(beanInterface, properties).newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	public static interface HasCookie {
		void __cookie__(Object cookie);

		Object __cookie__();
	}

	public static <T> Class<? extends T> makeDynamicProxyClass(Class<T> origin, Object interceptor,
			Class<?>... features) {
		Builder<T> builder = new ByteBuddy().subclass(origin).implement(features)
				.method(ElementMatchers.isDeclaredBy(origin)).intercept(MethodDelegation.to(interceptor));
		for (Class<?> feature : features) {
			if (feature == HasCookie.class) {
				builder = builder.defineField("__cookie__", Object.class, Visibility.PUBLIC);
				try {
					builder = builder.define(HasCookie.class.getMethod("__cookie__", Object.class))
							.intercept(FieldAccessor.ofField("__cookie__"));
					builder = builder.define(HasCookie.class.getMethod("__cookie__"))
							.intercept(FieldAccessor.ofField("__cookie__"));
				} catch (NoSuchMethodException | SecurityException e) {
					throw new RuntimeException(e);
				}
			}
		}
		return builder.make().load(origin.getClassLoader(), ClassLoadingStrategy.Default.WRAPPER).getLoaded();
	}

	// @Origin Method method

	private static <T> Class<? extends T> implementInteface(Class<T> beanInterface,
			Collection<BeanMaker.PropertyDetails> properties) {
		Builder<Object> implement = new ByteBuddy().subclass(Object.class).implement(beanInterface);
		for (BeanMaker.PropertyDetails propertyDetails : properties) {
			implement = implement.defineField(propertyDetails.name, propertyDetails.getType(), Visibility.PUBLIC);
			if (propertyDetails.setter != null) {
				implement = implement.define(propertyDetails.setter)
						.intercept(FieldAccessor.ofField(propertyDetails.name));
			} else {
				implement = implement
						.defineMethod(propertyDetails.name, void.class, Visibility.PUBLIC, Ownership.MEMBER)
						.withParameter(propertyDetails.getType())
						.intercept(FieldAccessor.ofField(propertyDetails.name));
			}
			if (propertyDetails.getter != null) {
				implement = implement.define(propertyDetails.getter)
						.intercept(FieldAccessor.ofField(propertyDetails.name));
			} else {
				implement = implement.defineMethod(propertyDetails.name, propertyDetails.getType(), Visibility.PUBLIC,
						Ownership.MEMBER).intercept(FieldAccessor.ofField(propertyDetails.name));
			}
		}
		final Unloaded<Object> make = implement.make();
		final Class<? extends Object> loaded = make
				.load(beanInterface.getClassLoader(), ClassLoadingStrategy.Default.WRAPPER).getLoaded();
		return (Class<? extends T>) loaded;
	}

	private static <T> Collection<BeanMaker.PropertyDetails> calcProperties(Class<T> beanInterface) {
		LinkedHashMap<String, BeanMaker.PropertyDetails> propertiesMap = new LinkedHashMap<>();
		colloectMethods(beanInterface, propertiesMap);
		Collection<BeanMaker.PropertyDetails> properties = propertiesMap.values();
		return properties;
	}

	private static <T> void colloectMethods(Class<T> beanInterface,
			LinkedHashMap<String, BeanMaker.PropertyDetails> propertiesMap) {
		Method[] methods = beanInterface.getMethods();
		for (int i = 0; i < methods.length; i++) {
			Method method = methods[i];
			if (Modifier.isStatic(method.getModifiers()))
				continue;
			if (method.isDefault())
				continue;
			if (method.getReturnType().equals(void.class)) {
				if (method.getParameterCount() == 1) {
					// setter
					String name = QueryUtils.getPropertyNameFromSetter(method);
					BeanMaker.PropertyDetails property = getPropertyMetadata(propertiesMap, name);
					property.setSetter(method);
				}
				continue;
			} else {
				if (method.getParameterCount() == 0) {
					// getter
					String name = QueryUtils.getPropertyNameFromGetter(method);
					BeanMaker.PropertyDetails property = getPropertyMetadata(propertiesMap, name);
					property.setGetter(method);
				}
			}
		}
	}

	private static BeanMaker.PropertyDetails getPropertyMetadata(
			LinkedHashMap<String, BeanMaker.PropertyDetails> propertiesMap, String propertyName) {
		BeanMaker.PropertyDetails property = propertiesMap.get(propertyName);
		if (property == null) {
			property = new BeanMaker.PropertyDetails(propertyName);
			propertiesMap.put(propertyName, property);
		}
		return property;
	}
	public static <GetterType> GetterType makeGetter(Method getterMethod, Class<GetterType> getterType)
			throws Throwable {
		MethodHandles.Lookup caller = MethodHandles.lookup();
		MethodHandle getterHandle = caller.unreflect(getterMethod);
	    Class<?> returnType = getterType.getMethod("get",Object.class).getReturnType();
		
		CallSite site = LambdaMetafactory.metafactory(caller, "get", MethodType.methodType(getterType),
				MethodType.methodType(returnType, Object.class), getterHandle, getterHandle.type());

		MethodHandle factory = site.getTarget();
		GetterType getter = (GetterType) factory.invoke();
		return getter;
	}

	public static <SetterType> SetterType makeSetter(Method setterMethod, Class<SetterType> setterType)
			throws Throwable {
		MethodHandles.Lookup caller = MethodHandles.lookup();
		MethodHandle setterHandle = caller.unreflect(setterMethod);

	    Class<?> valueType = setterType.getDeclaredMethods()[0].getParameterTypes()[1];
		
		CallSite site = LambdaMetafactory.metafactory(caller, "set", MethodType.methodType(setterType),
				MethodType.methodType(setterMethod.getReturnType(), Object.class, valueType),
				setterHandle, setterHandle.type());

		MethodHandle factory = site.getTarget();
		SetterType setter = (SetterType) factory.invoke();
		return setter;
	}

	private static final Map<Class<?>, Class<?>> PRIMITIVES_TO_WRAPPERS = makeWrappersMap();

	private static Map<Class<?>, Class<?>> makeWrappersMap() {
		Map<Class<?>, Class<?>> map = new HashMap<Class<?>, Class<?>>();
		map.put(boolean.class, Boolean.class);
		map.put(byte.class, Byte.class);
		map.put(char.class, Character.class);
		map.put(double.class, Double.class);
		map.put(float.class, Float.class);
		map.put(int.class, Integer.class);
		map.put(long.class, Long.class);
		map.put(short.class, Short.class);
		map.put(void.class, Void.class);
		return map;
	}

	@SuppressWarnings("unchecked")
	public static <T> Class<T> wrap(Class<T> c) {
		return c.isPrimitive() ? (Class<T>) PRIMITIVES_TO_WRAPPERS.get(c) : c;
	}

}