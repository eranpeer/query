package org.query;

import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

public final class DbRowImpl implements DbValue {
	private ResultSet rs;
	public int colIndex;

	public DbRowImpl(ResultSet rs) {
		this.rs = rs;
	}

	@Override
	public String asString() throws SQLException {
		return rs.getString(colIndex);
	}

	@Override
	public byte asShort() throws SQLException {
		return rs.getByte(colIndex);
	}

	@Override
	public long asLong() throws SQLException {
		return rs.getLong(colIndex);
	}

	@Override
	public int asInt() throws SQLException {
		return rs.getInt(colIndex);
	}

	@Override
	public Integer asIntWrapper() throws Exception {
		int val = rs.getInt(colIndex);
		if (val == 0 && rs.wasNull())
			return null;
		return val;
	}

	@Override
	public Long asLongWrapper() throws Exception {
		long val = rs.getLong(colIndex);
		if (val == 0 && rs.wasNull())
			return null;
		return val;
	}

	@Override
	public float asFloat() throws SQLException {
		return rs.getFloat(colIndex);
	}

	@Override
	public double asDouble() throws SQLException {
		return rs.getDouble(colIndex);
	}

	@Override
	public byte asByte() throws SQLException {
		return rs.getByte(colIndex);
	}

	@Override
	public boolean asBoolean() throws SQLException {
		return rs.getBoolean(colIndex);
	}

	@Override
	public Timestamp asTimestamp() throws SQLException {
		return rs.getTimestamp(colIndex);
	}

	public java.sql.Date asDate() throws SQLException {
		return rs.getDate(colIndex);
	}

	public LocalDateTime asLocalDateTime() throws SQLException {
		final Timestamp date = rs.getTimestamp(colIndex);
		if (date == null)
			return null;
		return date.toLocalDateTime();
	}
	
	public LocalDate asLocalDate() throws SQLException {
		final Timestamp date = rs.getTimestamp(colIndex);
		if (date == null)
			return null;
		return date.toLocalDateTime().toLocalDate();
	}

	public java.util.Date asJavaDate() throws SQLException {
		return new java.util.Date(rs.getTimestamp(colIndex).getTime());
	}

	public java.sql.Time asTime() throws SQLException {
		return rs.getTime(colIndex);
	}

	public Blob asBlob() throws SQLException {
		return rs.getBlob(colIndex);
	}

	public byte[] asBytes() throws SQLException {
		return rs.getBytes(colIndex);
	}

}