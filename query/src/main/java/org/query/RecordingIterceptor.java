package org.query;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.Consumer;

import net.bytebuddy.implementation.bind.annotation.Origin;
import net.bytebuddy.implementation.bind.annotation.RuntimeType;
import net.bytebuddy.implementation.bind.annotation.This;

public class RecordingIterceptor implements MethodInvocationInterceptor {

	private Consumer<PropertyMethodInvocation> lastCalledMethodConsumer;

	RecordingIterceptor(Consumer<PropertyMethodInvocation> lastMethodCallConsumer) {
		this.lastCalledMethodConsumer = lastMethodCallConsumer;
	}

	@RuntimeType
	public Object intercept(@This Object thiz, @Origin Method method) {
		lastCalledMethodConsumer.accept(new PropertyMethodInvocation(thiz, method));
		return makeDefaultReturnValue(method);
	}

	private Object makeDefaultReturnValue(Method method) {
		Class<?> returnType = method.getReturnType();
		if (!returnType.isPrimitive())
			return null;
		Constructor<?> constructor;
		try {
			constructor = BeanMaker.wrap(returnType).getConstructor(String.class);
			Object rv = constructor.newInstance("0");
			return rv;
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void intercept(Object thiz, Method method, Object argument) {
		lastCalledMethodConsumer.accept(new PropertyMethodInvocation(thiz, method, argument));
	}
}