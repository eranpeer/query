package org.query;

import java.util.ArrayList;
import java.util.List;

public class SelectStatement1<T1> extends SelectStatementBase {

	public static class ExecutableStatement<T1> {

		private final JdbcQueryEngine engine;
		private final ParsedStatement parsedStatement;
		private DbEntityMetadata t1Metadata;
		private PropertyMetadata[] selectedProperties;

		public ExecutableStatement(JdbcQueryEngine engine, DbEntityMetadata t1Metadata,
				PropertyMetadata[] selectedProperties, ParsedStatement parsedStatement) {
			this.engine = engine;
			this.t1Metadata = t1Metadata;
			this.selectedProperties = selectedProperties;
			this.parsedStatement = parsedStatement;
		}

		public List<T1> findAll() {
			return engine.query(//
					parsedStatement.getStatementSql(), //
					parsedStatement.getArguments(), //
					rs -> collectAll(rs, t1Metadata, selectedProperties)//
			);
		}

		public <Data> List<Data> findAll(TypedRowMapper<T1, Data> rowMapper) {
			return engine.query(//
					parsedStatement.getStatementSql(), //
					parsedStatement.getArguments(), //
					(rs) -> {
						ArrayList<Data> result = new ArrayList<Data>();
						try {
							@SuppressWarnings("unchecked")
							T1 e = (T1) t1Metadata.getEntityType().newInstance();
							PropertyMetadata[] quieriedProperties = QueryUtils.calcQueriedProperties(rs, t1Metadata);
							final DbRowImpl dbRow = new DbRowImpl(rs);
							while (rs.next()) {
								QueryUtils.populate(e, dbRow, quieriedProperties);
								result.add(rowMapper.map(e));
							}
							return result;
						} catch (Exception e) {
							throw new RuntimeException(e);
						}
					});
		}

		public T1 findOne() {
			return engine.query(//
					parsedStatement.getStatementSql(), //
					parsedStatement.getArguments(), //
					rs -> collectOne(rs, t1Metadata, selectedProperties)//
			);
		}
	}

	public interface TypedRowMapper<T1, Data> {
		Data map(T1 e1);
	}

	private final JdbcQueryEngine engine;
	private WhereClauseRecorder<T1> filter;
	private DbEntityMetadata t1Metadata;

	public SelectStatement1(JdbcQueryEngine engine, Class<T1> t1) {
		this.engine = engine;
		this.t1Metadata = engine.getMetadata(t1);
	}

	public List<T1> findAll() {
		return makeExecutableStatement().findAll();
	}

	public <Data> List<Data> findAll(TypedRowMapper<T1, Data> rowMapper) {
		return makeExecutableStatement().findAll(rowMapper);
	}

	public ExecutableStatement<T1> makeExecutableStatement() {
		StringBuilder statementText = new StringBuilder();
		statementText.append("select ");
		final PropertyMetadata[] selectedProperties = t1Metadata.getProperties();
		QueryUtils.appendSelectedColumns(statementText, selectedProperties);
		statementText.append(" from ").append(t1Metadata.getFormattedTableName());
		ArrayList<Object> queryParams = new ArrayList<>();
		if (filter != null) {
			statementText.append("\nwhere ");
			SqlElement wc = WhereClauseUtils.recordWhereClause(filter, t1Metadata);
			wc.apply(statementText, queryParams);
		}
		ParsedStatement parsedStatement = new ParsedStatement(statementText.toString(), queryParams.toArray());
		return new ExecutableStatement<>(engine, t1Metadata, selectedProperties, parsedStatement);
	}

	public SelectStatement1<T1> where(WhereClauseRecorder<T1> filter) {
		this.filter = filter;
		return this;
	}

	public T1 findOne() {
		return makeExecutableStatement().findOne();
	}
}