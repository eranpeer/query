package org.query;

import java.lang.invoke.CallSite;
import java.lang.invoke.LambdaConversionException;
import java.lang.invoke.LambdaMetafactory;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.function.IntBinaryOperator;
import java.util.function.IntSupplier;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.modifier.Visibility;
import net.bytebuddy.dynamic.loading.ClassLoadingStrategy;
import net.bytebuddy.implementation.FieldAccessor;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.implementation.bind.annotation.Origin;
import net.bytebuddy.implementation.bind.annotation.RuntimeType;
import net.bytebuddy.implementation.bind.annotation.SuperCall;
import net.bytebuddy.matcher.ElementMatchers;

public class TestMethodPerf {
	private static final int ITERATIONS = 50_000_000;
	private static final int WARM_UP = 10;

	public static interface MyBean {
		public int sum(int a, int b);
	}

	public static class Bean {

		public int sum(int a, int b) {
			return a + b;
		}
	}

	public static interface MyBean2 {
		int getAge();

		void setAge(int value);
	}

	public static class MyBean2Impl implements MyBean2 {
		private int age;

		@Override
		public int getAge() {
			return age;
		}

		@Override
		public void setAge(int value) {
			age = value;
		}
	}

	public static class Iterceptor {
		@RuntimeType
		public Object intercept(@Origin Method method) {
			return 2;
		}
	}
 
	static MyBean a;
	private static MyBean2 b;
	private static MyBean2 bDirect = new MyBean2Impl();
	static Field f;
	static {
		try {
			a = (MyBean) new ByteBuddy().subclass(Object.class).implement(MyBean.class)
					.method(ElementMatchers.isDeclaredBy(MyBean.class))
					.intercept(MethodDelegation.to(TestMethodPerf.class)).make()
					.load(TestMethodPerf.class.getClassLoader(), ClassLoadingStrategy.Default.WRAPPER).getLoaded()
					.newInstance();
			// a = new ByteBuddy().subclass(MyBean.class).make()
			// .load(TestMethodPerf.class.getClassLoader(),
			// ClassLoadingStrategy.Default.WRAPPER).getLoaded()
			// .newInstance();

			MyBean2 b2 = BeanMaker.makeBeanFromInterface(MyBean2.class);
			b2.setAge(5);
			int age = b2.getAge();
			MyBean2 b3 = BeanMaker.makeDynamicProxyClass(b2.getClass(), new Iterceptor()).newInstance();
			age = b3.getAge();
			b = (MyBean2) new ByteBuddy().subclass(Object.class).implement(MyBean2.class)
					.defineField("age", Integer.TYPE, Visibility.PUBLIC)
					.define(MyBean2.class.getMethod("setAge", int.class)).intercept(FieldAccessor.ofField("age"))//
					.define(MyBean2.class.getMethod("getAge")).intercept(FieldAccessor.ofField("age")).make()
					.load(TestMethodPerf.class.getClassLoader(), ClassLoadingStrategy.Default.WRAPPER).getLoaded()
					.newInstance();

			f = b.getClass().getField("age");
		} catch (InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException
				| NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String... args) throws Throwable {
		// http://stackoverflow.com/questions/27602758/java-access-bean-methods-with-lambdametafactory
		MyBean2Impl simpleBeanInstance = new MyBean2Impl();
		simpleBeanInstance.setAge(2);
		optionOne(simpleBeanInstance);
		optionTwo(simpleBeanInstance);
		optionThree(simpleBeanInstance);
		optionFour(simpleBeanInstance);
		// assertEquals( "myCustomObject", r.get());
	}

	private static void optionOne(MyBean2Impl simpleBeanInstance)
			throws NoSuchMethodException, IllegalAccessException, LambdaConversionException, Throwable {
		MethodHandles.Lookup caller = MethodHandles.lookup();
		MethodType getter = MethodType.methodType(int.class);
		MethodHandle target = caller.findVirtual(MyBean2Impl.class, "getAge", getter);
		// Method reflected = MyBean2Impl.class.getDeclaredMethod("getAge");
		// MethodHandle target = caller.unreflect(reflected);

		CallSite site = LambdaMetafactory.metafactory(caller, "getAsInt",
				MethodType.methodType(IntSupplier.class, MyBean2Impl.class), getter, target, getter);

		MethodHandle factory = site.getTarget();
		factory = factory.bindTo(simpleBeanInstance);
		IntSupplier r = (IntSupplier) factory.invoke();
		r.getAsInt();
	}

	private static void optionTwo(MyBean2Impl simpleBeanInstance)
			throws NoSuchMethodException, IllegalAccessException, LambdaConversionException, Throwable {
		MethodHandles.Lookup caller = MethodHandles.lookup();
		// MethodType getter = MethodType.methodType(int.class);
		// MethodHandle target=caller.findVirtual(MyBean2Impl.class, "getAge",
		// getter);

		Method reflected = MyBean2Impl.class.getDeclaredMethod("getAge");

		MethodHandle target = caller.unreflect(reflected);
		MethodType func = target.type();
		CallSite site = LambdaMetafactory.metafactory(caller, "apply", MethodType.methodType(Function.class),
				func.generic(), target, func);

		MethodHandle factory = site.getTarget();
		Function r = (Function) factory.invoke();
		Object val = r.apply(simpleBeanInstance);
	}
	
	private static void optionFour(MyBean2Impl simpleBeanInstance)
			throws NoSuchMethodException, IllegalAccessException, LambdaConversionException, Throwable {
		Method getterMethod = MyBean2Impl.class.getDeclaredMethod("getAge");
		Method setterMethod = MyBean2Impl.class.getDeclaredMethod("setAge",int.class);
		
		IntSetter s = BeanMaker.makeSetter(setterMethod, IntSetter.class);
		IntGetter g = BeanMaker.makeGetter(getterMethod, IntGetter.class);
		s.set(simpleBeanInstance, 5);
		int val = g.get(simpleBeanInstance);
	}

	

	private static void optionThree(MyBean2Impl simpleBeanInstance)
			throws NoSuchMethodException, IllegalAccessException, LambdaConversionException, Throwable {
		MethodHandles.Lookup caller = MethodHandles.lookup();

		Method reflected = MyBean2Impl.class.getDeclaredMethod("getAge");
		MethodHandle target = caller.unreflect(reflected);
		MethodType getterPrototype = MethodType.methodType(reflected.getReturnType());

		CallSite site = LambdaMetafactory.metafactory(caller, "getAsInt",
				MethodType.methodType(IntSupplier.class, MyBean2Impl.class), getterPrototype, target, getterPrototype);

		MethodHandle factory = site.getTarget();
		factory = factory.bindTo(simpleBeanInstance);
		IntSupplier r = (IntSupplier) factory.invoke();
		r.getAsInt();
	}

	private void runTest() throws NoSuchMethodException, IllegalAccessException, LambdaConversionException, Throwable {

		// System.out.printf(
		// "direct: %.4fs, lambda: %.4fs, mh: %.2fs, reflection: %.2fs,
		// bytebuddy:%.2fs, bean:%.4fs, bytebuddy bean:%.4fs%n",
		// (t1 - t0) * 1e-9, (t2 - t1) * 1e-9, (t3 - t2) * 1e-9, (t4 - t3) *
		// 1e-9, (t5 - t4) * 1e-9,
		// (t6 - t5) * 1e-9, (t7 - t6) * 1e-9);
		//
		// // do something with the results
		// if (dummy[0] != dummy[1] || dummy[0] != dummy[2] || dummy[0] !=
		// dummy[3] || dummy[5] != dummy[6])
		// System.out.println(1);
		// else
		// System.out.println(2);
	}

	private int testBean(int v, MyBean2 b) {
		for (int i = 0; i < ITERATIONS; i++) {
			b.setAge(b.getAge() + v);
			v += b.getAge();
		}

		return v;
	}

	private int testBean2(int v, MyBean2 b) throws IllegalArgumentException, IllegalAccessException {
		for (int i = 0; i < ITERATIONS; i++) {
			f.set(b, (int) f.get(b) + v);
			v += (int) f.get(b);
		}

		return v;
	}

	private int testMH(int v, MethodHandle mh) throws Throwable {
		for (int i = 0; i < ITERATIONS; i++)
			v += (int) mh.invokeExact(1000, v);
		return v;
	}

	private int testReflection(int v, Method mh) throws Throwable {
		for (int i = 0; i < ITERATIONS; i++)
			v += (int) mh.invoke(null, 1000, v);
		return v;
	}

	private int testDirect(int v) {
		for (int i = 0; i < ITERATIONS; i++)
			v += myMethod(1000, v);
		return v;
	}

	private int testLambda(int v, IntBinaryOperator accessor) {
		for (int i = 0; i < ITERATIONS; i++)
			v += accessor.applyAsInt(1000, v);
		return v;
	}

	private int testByteBuddy(int v) {
		for (int i = 0; i < ITERATIONS; i++)
			v += a.sum(1000, v);
		return v;
	}

	public static int myMethod(int a, int b) {
		return a < b ? a : b;
	}

	public int myMethodVirtual(int a, int b) {
		return a < b ? a : b;
	}

}