package org.query;

import java.time.LocalDate;

public interface LocalDateGetter {
	public LocalDate get(Object thiz);
}