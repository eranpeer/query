package org.query;

import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.persistence.Table;

public class QueryUtils {

	public static String getPropertyNameFromGetter(Method method) {
		return getPropertyName(method, "get");
	}

	private static String getPropertyName(Method method, final String prefix) {
		String result = method.getName().toLowerCase();
		if (result.startsWith(prefix))
			result = result.substring(3);
		return result;
	}

	public static String getPropertyNameFromSetter(Method method) {
		return getPropertyName(method, "set");
	}

	public static PropertyMetadata[] calcQueriedProperties(ResultSet rs, DbEntityMetadata dbEntityMetadata)
			throws SQLException {
		PropertyMetadata[] quieriedProperties = new PropertyMetadata[rs.getMetaData().getColumnCount()];
		for (int i = 1; i <= quieriedProperties.length; i++) {
			String columnLabel = rs.getMetaData().getColumnLabel(i);
			quieriedProperties[i - 1] = dbEntityMetadata.getPropertyMetadata(columnLabel);
		}
		return quieriedProperties;
	}

	public static String[] getColumNames(ResultSet rs) throws SQLException {
		String columnNames[] = new String[rs.getMetaData().getColumnCount()];
		for (int i = 0; i < columnNames.length; i++) {
			columnNames[i] = rs.getMetaData().getColumnName(i + 1);
		}
		return columnNames;
	}

	public static void populate(Object e, DbRowImpl dbRow, PropertyMetadata[] properties) throws Exception {
		for (dbRow.colIndex = 1; dbRow.colIndex <= properties.length; dbRow.colIndex++) {
			final PropertyMetadata property = properties[dbRow.colIndex - 1];
			if (property.getDbEntityMetadata().getEntityType().isInstance(e))
				property.setValue(e, dbRow);
		}
	}

	public static void appendColumnNameAndLable(StringBuilder statementText, PropertyMetadata propertyMetadata) {
		statementText.append(propertyMetadata.getFormattedFullColumnName()).append(" as ").append('"')
				.append(propertyMetadata.getColumnLabel()).append('"');
	}

	public static void appendSelectedColumns(StringBuilder statementText, PropertyMetadata[] properties) {
		for (PropertyMetadata propertyMetadata : properties) {
			appendColumnNameAndLable(statementText, propertyMetadata);
			statementText.append(',');
		}
		if (properties.length > 0)
			statementText.setLength(statementText.length() - 1);
	}

	public static void appendInsertedColumns(StringBuilder statementText, PropertyMetadata[] properties) {
		for (PropertyMetadata propertyMetadata : properties) {
			statementText.append(propertyMetadata.getFormattedColumnName());
			statementText.append(',');
		}
		if (properties.length > 0)
			statementText.setLength(statementText.length() - 1);
	}

}