package org.query;

import java.sql.PreparedStatement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementCreator;

public class SpringJdbcQueryEngine implements JdbcQueryEngine {

	private static final Logger log = LoggerFactory.getLogger(SpringJdbcQueryEngine.class);

	private final JdbcOperations jdbcOperations;
	private final SqlDialect sqlDialect;

	public SpringJdbcQueryEngine(JdbcOperations jdbcOperations, SqlDialect sqlDialect) {
		this.jdbcOperations = jdbcOperations;
		this.sqlDialect = sqlDialect;
	}

	@Override
	public <T> T execute(PreparedStatementProducer psp, PreparedStatementConsumer<T> psc) {
		return jdbcOperations.execute(//
				(PreparedStatementCreator) con -> {
					PreparedStatement ps = psp.produce(con);
					log.info("executing: {}" + ps);
					return ps;
				}, //
				(PreparedStatementCallback<T>) ps -> psc.consume(ps));
	}

	@Override
	public DbEntityMetadata getMetadata(Class<?> t1) {
		return DbEntityMetadata.make(t1, sqlDialect);
	}

}